package com.example.webTEST;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MainPageTest {

    protected WebDriver driver;
    protected String email;

    @BeforeClass
    public void openPage() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://users.bugred.ru/user/login/index.html");
        driver.manage().window().maximize();
    }

    @Test(priority = 1)
    public void signUpTest() {
        driver.findElement(By.xpath("//body[1]/div[3]/div[1]/div[2]/form[1]/table[1]/tbody[1]/tr[1]/td[2]/input[1]")).
                sendKeys(generateName());
        driver.findElement(By.xpath("//body[1]/div[3]/div[1]/div[2]/form[1]/table[1]/tbody[1]/tr[2]/td[2]/input[1]")).
                sendKeys(generateEmail());
        driver.findElement(By.xpath("//body[1]/div[3]/div[1]/div[2]/form[1]/table[1]/tbody[1]/tr[3]/td[2]/input[1]")).
                sendKeys("123456test");

        driver.findElement(By.xpath("//tbody/tr[4]/td[2]/input[1]")).click();
    }

    @Test(priority = 2)
    public void exitTest() {
        driver.findElement(By.xpath("//body/div[1]/div[2]/ul[1]/li[3]/a[1]")).click();
        driver.findElement(By.xpath("//a[contains(text(),'Выход')]"));
    }

    @Test(priority = 3)
    public void signInTest() {
        driver.get("http://users.bugred.ru/user/login/index.html");

        driver.findElement(By.xpath("//body[1]/div[3]/div[1]/div[1]/form[1]/table[1]/tbody[1]/tr[1]/td[2]/input[1]")).
                sendKeys(email);
        driver.findElement(By.xpath("//body[1]/div[3]/div[1]/div[1]/form[1]/table[1]/tbody[1]/tr[2]/td[2]/input[1]")).
                sendKeys("123456test");

        driver.findElement(By.xpath("//body[1]/div[3]/div[1]/div[1]/form[1]/table[1]/tbody[1]/tr[3]/td[2]/input[1]")).click();
    }

    @Test(priority = 4)
    public void inputUserDataTest() {
        driver.findElement(By.xpath("//body/div[1]/div[2]/ul[1]/li[3]/a[1]")).click();
        driver.findElement(By.xpath("//a[contains(text(),'Личный кабинет')]")).click();

        driver.findElement(By.xpath("//tbody/tr[3]/td[2]/select[1]")).click();
        driver.findElement(By.xpath("//option[contains(text(),'Мужской')]")).click();

        driver.findElement(By.xpath("//tbody/tr[4]/td[2]/input[1]")).sendKeys("04.12.1998");
        driver.findElement(By.xpath("//tbody/tr[5]/td[2]/input[1]")).sendKeys("20.12.2020");

        driver.findElement(By.xpath("//tbody/tr[6]/td[2]/textarea[1]")).sendKeys("Some data about my hobbies");

        driver.findElement(By.xpath("//tbody/tr[7]/td[2]/input[1]")).sendKeys("123456789102");

        driver.findElement(By.xpath("//tbody/tr[8]/td[2]/input[1]")).click();
    }

    @AfterMethod
    public void TakeSnapShot() throws Exception {
        int n = 0;

        try {
            File theDir = new File("screenshots/test" + n);
            do {
                if (theDir.exists()) {
                    n = n + 1;
                    theDir = new File("screenshots/test" + n);
                }
                else { theDir.mkdir(); }
            } while (!theDir.exists());

            File screenShot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            int random = new Random().nextInt(200);
            FileUtils.copyFile(screenShot, new File(theDir.getAbsolutePath() + "/test" + random + ".jpeg"));
        }
        catch (SecurityException se) {
            System.out.println(se.getMessage());
        }
    }

    @AfterTest
    public void quit() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.quit();
    }

    private String generateEmail() {
        int random = new Random().nextInt(200);

        email = "test" + random + "@testmail.com";
        return email;
    }
    private String generateName() {
        int random = new Random().nextInt(200);

        String name;
        name = "TestName" + random;
        return name;
    }
}